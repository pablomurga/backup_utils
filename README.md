
This is a repository for various backup utilities

# Installation (Ubuntu)
~~~

apt-get update -y

apt-get install python-pip git -y

pip install paramiko, scp

git clone https://pablomurga@bitbucket.org/pablomurga/backup_utils.git

~~~

# Configure logrotate for backup logs (Ubuntu 16.04)
create file /etc/logrotate.d/backup-logs with the following template /etc/logrotate.d/backup-logs
~~~
/home/ubuntu/backup_utils/*.log {
    daily
    missingok
    rotate 7
    compress
    notifempty
    create 0640 ubuntu ubuntu
    sharedscripts
    endscript
}
~~~

# Usage

For F5 backups, complete the f5_inventory.txt file with `<HOSTNAME> <IP ADDRESS>`, leaving a space between the values, like so:

~~~
F5-Hostname-01 1.2.3.4
F5-Hostname-02 5.6.7.8
~~~


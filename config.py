import datetime
import logging

#Global config
date = datetime.datetime.now().date()

#Setup logging
logfile = "backup.log"
logging.basicConfig(filename=logfile, level=logging.INFO, format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

#Environment variables for f5 backup
f5_user = ''
f5_password = ''
f5_ssh_port =  ''
f5_inventory = 'f5_inventory.txt'

#Environment variables for s3 upload
s3_bucket = ''
import boto3
from botocore.exceptions import ClientError
import logging
import threading
import os

log = logging.getLogger(__name__)

class S3AsyncUploader(threading.Thread):

    def __init__(self, file_name, bucket, object_name):
        threading.Thread.__init__(self)
        self.s3_client = boto3.client('s3')
        self.file_name = file_name
        self.bucket = bucket
        self.object_name = object_name

    def run(self):
        response = self.upload_file(self.file_name, self.bucket, self.object_name)
        if response:
            self.delete_local_file(self.file_name)
        else:
            log.error("------ S3 upload ERROR ------")
            log.error("File {} was not deleted because S3 upload failed.".format(self.file_name))
            log.error("------ S3 upload ERROR ------")

    
    def upload_file(self, file_name, bucket, object_name):
        try:
            response = self.s3_client.upload_file(file_name, bucket, object_name)
            log.info("------ S3 upload SUCCESS ------")
            log.info("File uploaded successfully to {} as {}".format(bucket, object_name))
            return True
        except ClientError as e:
            log.error("------ S3 upload ERROR ------")
            log.error(e)
            log.error("------ S3 upload ERROR ------")
            return False

    def delete_local_file(self, file_name):
        if os.path.exists(file_name):
            os.remove(file_name)
            log.info("File {} was deleted successfully".format(file_name))
            log.info("------ S3 upload SUCCESS ------")
        else:
            log.error("------ S3 upload ERROR ------")
            log.error("File {} does not exist".format(file_name))
            log.error("------ S3 upload ERROR ------")

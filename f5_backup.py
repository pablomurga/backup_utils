import paramiko
from scp import SCPClient
import logging
import time
import config
import s3_upload_files

log = logging.getLogger(__name__)

class F5Backup():

    def __init__(self, host, ip):
        self.host = host
        self.ip = ip

    def run(self):
        bkp_file= "{}_{}_{}.ucs".format(self.host,self.ip,config.date)

        log.info("=====Triggered F5 Backup for {}_{}=====".format(self.host,self.ip))      
        ssh = self.open_ssh_connection(host=self.host,host_ip=self.ip)

        log.info("Creating backup file on remote host")
        self.run_remote_command(ssh_session=ssh,cmd="tmsh save sys ucs {}".format(bkp_file))
	
        time.sleep(30)
        
        log.info("Fetching backup file")
        self.scp_file_from_host(ssh_session=ssh, filename="/var/local/ucs/{}".format(bkp_file))

        log.info("Cleaning up remote backup directory /var/local/ucs")
        self.run_remote_command(ssh_session=ssh, cmd="yes | rm /var/local/ucs/{}".format(bkp_file)) 

        s3_uploader = s3_upload_files.S3AsyncUploader(file_name=bkp_file, bucket=config.s3_bucket, object_name="f5/{}".format(bkp_file))
        s3_uploader.start()

        log.info("=====F5 Backup for {}_{} Done=====".format(self.host,self.ip))      

    def run_remote_command(self, ssh_session, cmd):

        stdin,stdout,stderr=ssh_session.exec_command(cmd)
        err_outlines=stderr.readlines()
        outlines=stdout.readlines()
        resp=''.join(outlines)
        err_resp=''.join(err_outlines)
        log.info(resp)
        log.info(err_resp)

    def open_ssh_connection(self, host, host_ip):
        try:
            log.info("Trying to connect to {}".format(host))
            ssh = paramiko.SSHClient()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(hostname=host_ip,port=config.f5_ssh_port,username=config.f5_user,password=config.f5_password)
            log.info("Connected to {}".format(host))
            return ssh
        except paramiko.AuthenticationException:
            log.error("Authentication failed when connecting to {}".format(config.ssh_host))

    def scp_file_from_host(self, ssh_session, filename):
        with SCPClient(ssh_session.get_transport()) as scp:
            scp.get(filename)
            log.info("Copied file {} to localhost".format(filename))

def get_f5_hosts_and_ips():
    result = []
    with open(config.f5_inventory) as f:
        for line in f:
            tmp = line.split()
            log.info("Splitting line as: {}".format(tmp))
            try:
                result.append((tmp[0],tmp[1]))
            except:
                log.error("Could not split line: {}".format(line))
                pass
    return result

if __name__ == "__main__":

    for f5 in get_f5_hosts_and_ips():
        try:
            f5_backup = F5Backup(host=f5[0],ip=f5[1])
            f5_backup.run()
        except:
            log.error("Error backing up {}_{}".format(f5[0],f5[1]))

